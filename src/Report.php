<?php

namespace Mbilozub\UkieReports\Laravel;

use Exception;
use GuzzleHttp\Client;
use InvalidArgumentException;
use Throwable;

class Report
{
	/**
	 * The payload version.
	 *
	 * @var string
	 */
	const PAYLOAD_VERSION = '1';

	/**
	 * @var Configuration
	 */
	protected $config;

	/**
	 * @var
	 */
	protected $stacktrace;

	/**
	 * @var
	 */
	protected $previous;

	/**
	 * The error name.
	 *
	 * @var string
	 */
	protected $name;

	/**
	 * The error message.
	 *
	 * @var string|null
	 */
	protected $message;

	/**
	 * The error severity.
	 *
	 * @var string|null
	 */
	protected $severity;

	/**
	 * The associated context.
	 *
	 * @var string|null
	 */
	protected $context;

	/**
	 * The grouping hash.
	 *
	 * @var string|null
	 */
	protected $groupingHash;

	/**
	 * The associated meta data.
	 *
	 * @var array[]
	 */
	protected $metaData = [];

	/**
	 * @var array
	 */
	protected $deviceData = [];

	/**
	 * @var array
	 */
	protected $payload = [];

	/**
	 * The associated user.
	 *
	 * @var array
	 */
	protected $user = [];

	/**
	 * The error time.
	 *
	 * @var string
	 */
	protected $time;

	/**
	 * Report constructor.
	 * @param string $api_secret
	 * @param Exception $exception
	 */
	public function __construct(string $api_secret, \Exception $exception)
	{
		$this->config = new Configuration($api_secret);
		$this->config->setAppType(app()->runningInConsole() ? 'Console' : 'HTTP');
		$this->config->setReleaseStage(app()->environment());
		$this->setPHPThrowable($exception);
		$this->setMetaData();
		$this->setDeviceData();
		$this->time = gmdate('Y-m-d\TH:i:s\Z');
	}

	/**
	 * Set the PHP throwable.
	 *
	 * @param \Throwable $throwable the throwable instance
	 *
	 * @throws \InvalidArgumentException
	 *
	 * @return $this
	 */
	protected function setPHPThrowable($throwable)
	{
		if (!$throwable instanceof Throwable && !$throwable instanceof Exception) {
			throw new InvalidArgumentException('The throwable must implement Throwable or extend Exception.');
		}

		$this->setName(get_class($throwable))
			->setMessage($throwable->getMessage())
			->setStacktrace(Stacktrace::fromBacktrace($this->config, $throwable->getTrace(), $throwable->getFile(),
				$throwable->getLine()));

		if (method_exists($throwable, 'getPrevious')) {
			$this->setPrevious($throwable->getPrevious());
		}

		return $this;
	}

	/**
	 * @param Stacktrace $stacktrace
	 * @return $this
	 */
	protected function setStacktrace(Stacktrace $stacktrace)
	{
		$this->stacktrace = $stacktrace;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getStacktrace()
	{
		return $this->stacktrace;
	}

	/**
	 * Set the previous throwable.
	 *
	 * @param \Throwable $throwable the previous throwable
	 *
	 * @return $this
	 */
	protected function setPrevious($throwable)
	{
		if ($throwable) {
			$this->previous = static::fromPHPThrowable($this->config, $throwable);
		}

		return $this;
	}

	/**
	 * Set the error name.
	 *
	 * @param string $name the error name
	 *
	 * @throws \InvalidArgumentException
	 *
	 * @return $this
	 */
	public function setName($name)
	{
		if (is_scalar($name) || method_exists($name, '__toString')) {
			$this->name = (string)$name;
		} else {
			throw new InvalidArgumentException('The name must be a string.');
		}

		return $this;
	}

	/**
	 * Get the error name.
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set the error message.
	 *
	 * @param string|null $message the error message
	 *
	 * @throws \InvalidArgumentException
	 *
	 * @return $this
	 */
	public function setMessage($message)
	{
		if ($message === null) {
			$this->message = null;
		} elseif (is_scalar($message) || method_exists($message, '__toString')) {
			$this->message = (string)$message;
		} else {
			throw new InvalidArgumentException('The message must be a string.');
		}

		return $this;
	}

	/**
	 * Get the error message.
	 *
	 * @return string|null
	 */
	public function getMessage()
	{
		return $this->message;
	}

	/**
	 * Set the error severity.
	 *
	 * @param string|null $severity the error severity
	 *
	 * @throws \InvalidArgumentException
	 *
	 * @return $this
	 */
	public function setSeverity($severity)
	{
		if (in_array($severity, ['error', 'warning', 'info', null], true)) {
			$this->severity = $severity;
		} else {
			throw new InvalidArgumentException('The severity must be either "error", "warning", or "info".');
		}

		return $this;
	}

	/**
	 * Get the error severity.
	 *
	 * @return string
	 */
	public function getSeverity()
	{
		return $this->severity ?? 'warning';
	}

	/**
	 * Set a context representing the current type of request, or location in code.
	 *
	 * @param string|null $context the current context
	 *
	 * @return $this
	 */
	public function setContext($context)
	{
		$this->context = $context;

		return $this;
	}

	/**
	 * Get the error context.
	 *
	 * @return string|null
	 */
	public function getContext()
	{
		if (!$this->context && request()->server('REQUEST_METHOD') && request()->server('REQUEST_URI')) {
			$this->context = request()->server('REQUEST_METHOD') . ' ' . strtok(request()->server('REQUEST_URI'), '?');
		}

		return $this->context;
	}

	/**
	 * Set the grouping hash.
	 *
	 * @param string|null $groupingHash the grouping hash
	 *
	 * @return $this
	 */
	public function setGroupingHash($groupingHash)
	{
		$this->groupingHash = $groupingHash;

		return $this;
	}

	/**
	 * Get the grouping hash.
	 *
	 * @return string|null
	 */
	public function getGroupingHash()
	{
		return $this->groupingHash;
	}

	/**
	 * Set the error meta data.
	 *
	 * @param array|null $customData
	 */
	public function setMetaData(array $customData = null)
	{
		$data = [
			'url' => request()->getRequestUri(),
			'httpMethod' => request()->method(),
			'params' => request()->query(),
			'clientIp' => request()->getClientIp(),
			'userAgent' => request()->server('HTTP_USER_AGENT'),
			'referrer' => request()->server('HTTP_REFERER'),
			'protocol' => request()->server('SERVER_PROTOCOL'),
			'request' => request()->all(),
			'headers' => request()->headers->all(),
			'hostname' => php_uname('n'),
			'time' => gmdate('Y-m-d\TH:i:s\Z')
		];

		if($customData) {
			$data['custom'] = $customData;
		}

		$this->metaData = $data;
	}

	/**
	 * Get the error meta data.
	 *
	 * @return array[]
	 */
	public function getMetaData()
	{
		return $this->metaData;
	}

	/**
	 * @param array $customData
	 */
	public function setPayloadData(array $customData)
	{
		$this->payload = $customData;
	}

	/**
	 * Get the error meta data.
	 *
	 * @return array[]
	 */
	public function getPayloadData()
	{
		return count($this->payload) ? $this->payload : null;
	}

	/**
	 * Set the error device data.
	 *
	 * @param array|null $customData
	 */
	public function setDeviceData(array $customData = null)
	{
		$data = [
			'name' => request()->server('HTTP_USER_AGENT'),
			'ip' => request()->getClientIp()
		];

		if(array_get($customData, 'os')) {
			$data['os'] = array_get($customData, 'os');
		}

		$this->deviceData = $data;
	}

	/**
	 * Get the error meta data.
	 *
	 * @return array[]
	 */
	public function getDeviceData()
	{
		return $this->deviceData;
	}

	/**
	 * Get the input params.
	 *
	 * Note how we're caching this result for ever, across all instances.
	 *
	 * This is because the input stream can only be read once on PHP 5.5, and
	 * PHP is natively only designed to process one request, then shutdown.
	 * Some applications can be designed to handle multiple requests using
	 * their own request objects, thus will need to implement their own UkieReport
	 * request resolver.
	 *
	 * @param array $post the post variables
	 *
	 * @return array|null
	 */
	protected function getInputParams()
	{
		static $result;

		if ($result !== null) {
			return $result ?: null;
		}

		$result = request()->request ?: $this->parseInput();

		return $result ?: null;
	}

	/**
	 * @return null
	 */
	protected function parseInput()
	{
		$input = file_get_contents('php://input') ?: false;

		if (!$input) {
			return null;
		}

		if (request()->server('CONTENT_TYPE') && stripos(request()->server('CONTENT_TYPE'), 'application/json') === 0) {
			return (array)json_decode($input, true) ?: null;
		}

		if (strtoupper(request()->server('REQUEST_METHOD')) === 'PUT') {
			parse_str($input, $params);

			return (array)$params ?: null;
		}
	}

	/**
	 * Set the current user.
	 *
	 * @param array $user the current user
	 *
	 * @return $this
	 */
	public function setUser(array $user)
	{
		$this->user = $user;

		return $this;
	}

	/**
	 * Get the current user.
	 *
	 * @return array
	 */
	public function getUser()
	{
		if(!$this->user && auth()->check() && auth()->user()) {
			$this->user = [
				'id' => auth()->user()->id,
				'email' => auth()->user()->email
			];
			if(auth()->user()->role) {
				$this->user['role'] = auth()->user()->role;
			}
		}

		if(!$this->user) {
			$this->user['ip'] = request()->getClientIp();
		}

		return $this->user;
	}

	/**
	 * Get the report summary.
	 *
	 * @return string[]
	 */
	public function getSummary()
	{
		$summary = [];

		$name = $this->getName();
		$message = $this->getMessage();

		if ($name !== $message) {
			$summary['name'] = $name;
		}

		$summary['message'] = $message;

		$summary['severity'] = $this->getSeverity();

		return array_filter($summary);
	}

	/**
	 * Get the array representation.
	 *
	 * @return array
	 */
	public function toArray()
	{
		$event = [
			'interface' => $this->config->getAppData()['type'],
			'device' => $this->getDeviceData(),
			'user' => $this->getUser(),
			'context' => $this->getContext(),
			'payload' => $this->getPayloadData(),
			'payload_v' => static::PAYLOAD_VERSION,
			'severity' => $this->getSeverity(),
			'exceptions' => $this->exceptionArray(),
			'metaData' => $this->cleanupObj($this->getMetaData(), true),
		];

		if ($hash = $this->getGroupingHash()) {
			$event['groupingHash'] = $hash;
		}

		return $event;
	}

	/**
	 * Get the exception array.
	 *
	 * @return array
	 */
	protected function exceptionArray()
	{
		$exceptionArray = [$this->exceptionObject()];
		$previous = $this->previous;
		while ($previous) {
			$exceptionArray[] = $previous->exceptionObject();
			$previous = $previous->previous;
		}

		return $this->cleanupObj($exceptionArray, false);
	}

	/**
	 * Get serializable representation of the exception causing this report.
	 *
	 * @return array
	 */
	protected function exceptionObject()
	{
		return [
			'errorClass' => $this->name,
			'message' => $this->message,
			'stacktrace' => $this->stacktrace->toArray(),
		];
	}

	/**
	 * Cleanup the given object.
	 *
	 * @param mixed $obj the data to cleanup
	 * @param bool $isMetaData if it is meta data
	 *
	 * @return array|null
	 */
	protected function cleanupObj($obj, $isMetaData)
	{
		if (is_null($obj)) {
			return;
		}

		if (is_array($obj)) {
			$clean = [];

			foreach ($obj as $key => $value) {
				$clean[$key] = $this->shouldFilter($key, $isMetaData) ? '[FILTERED]' : $this->cleanupObj($value,
					$isMetaData);
			}

			return $clean;
		}

		if (is_string($obj)) {
			return (function_exists('mb_detect_encoding') && !mb_detect_encoding($obj, 'UTF-8',
					true)) ? utf8_encode($obj) : $obj;
		}

		if (is_object($obj)) {
			return $this->cleanupObj(json_decode(json_encode($obj), true), $isMetaData);
		}

		return $obj;
	}

	/**
	 * Should we filter the given element.
	 *
	 * @param string $key the associated key
	 * @param bool $isMetaData if it is meta data
	 *
	 * @return bool
	 */
	protected function shouldFilter($key, $isMetaData)
	{
		if ($isMetaData) {
			foreach ($this->config->getFilters() as $filter) {
				if (strpos($key, $filter) !== false) {
					return true;
				}
			}
		}

		return false;
	}

	public function notify()
	{
		$client = new Client();
		$data = [
			'api_key' => $this->config->getApiKey(),
			'version' => '1.0',
			'type' => 'php',
			'stage' => $this->config->getAppData()['releaseStage'],
			'events' => [
				$this->toArray()
			]
		];

		$client->post('http://ukiereports.l/notify', [
			'json' => $data
		]);
	}
}
