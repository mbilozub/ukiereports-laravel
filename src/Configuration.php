<?php

namespace Mbilozub\UkieReports\Laravel;

use InvalidArgumentException;

class Configuration
{
	/**
	 * The API Key.
	 *
	 * @var string
	 */
	protected $apiKey;

	/**
	 * The strings to filter out from metaData.
	 *
	 * @var string[]
	 */
	protected $filters = ['password'];

	/**
	 * The project root regex.
	 *
	 * @var string
	 */
	protected $projectRootRegex;

	/**
	 * The strip path regex.
	 *
	 * @var string
	 */
	protected $stripPathRegex;

	/**
	 * The notifier to report as.
	 *
	 * @var string[]
	 */
	protected $notifier = [
		'name' => 'UkieReports PHP',
		'version' => '1.0'
	];

	/**
	 * The fallback app type.
	 *
	 * @var string|null
	 */
	protected $fallbackType;

	/**
	 * The application data.
	 *
	 * @var string[]
	 */
	protected $appData = [];

	/**
	 * The error reporting level.
	 *
	 * @var int|null
	 */
	protected $errorReportingLevel;

	/**
	 * Create a new config instance.
	 *
	 * @param string $apiKey your UkieReport api key
	 *
	 * @throws \InvalidArgumentException
	 *
	 * @return void
	 */
	public function __construct($apiKey)
	{
		if (!is_string($apiKey)) {
			throw new InvalidArgumentException('Invalid API key');
		}

		$this->apiKey = $apiKey;
		$this->fallbackType = php_sapi_name();
		$this->setProjectRoot(base_path());
	}

	/**
	 * @return string
	 */
	public function getApiKey()
	{
		return $this->apiKey;
	}

	/**
	 * Set the strings to filter out from metaData arrays before sending then.
	 *
	 * Eg. ['password', 'credit_card'].
	 *
	 * @param string[] $filters an array of metaData filters
	 *
	 * @return $this
	 */
	public function setFilters(array $filters)
	{
		$this->filters = $filters;

		return $this;
	}

	/**
	 * Get the array of metaData filters.
	 *
	 * @return string[]
	 */
	public function getFilters()
	{
		return $this->filters;
	}

	/**
	 * Set the project root.
	 *
	 * @param string|null $projectRoot the project root path
	 *
	 * @return void
	 */
	public function setProjectRoot($projectRoot)
	{
		$this->projectRootRegex = $projectRoot ? '/^' . preg_quote($projectRoot, '/') . '[\\/]?/i' : null;

		if ($projectRoot && !$this->stripPathRegex) {
			$this->setStripPath($projectRoot);
		}
	}

	/**
	 * Is the given file in the project?
	 *
	 * @param string $file
	 *
	 * @return string
	 */
	public function isInProject($file)
	{
		return $this->projectRootRegex && preg_match($this->projectRootRegex, $file);
	}

	/**
	 * Set the strip path.
	 *
	 * @param string|null $stripPath the absolute strip path
	 *
	 * @return void
	 */
	public function setStripPath($stripPath)
	{
		$this->stripPathRegex = $stripPath ? '/^' . preg_quote($stripPath, '/') . '[\\/]?/i' : null;
	}

	/**
	 * Set the stripped file path.
	 *
	 * @param string $file
	 *
	 * @return string
	 */
	public function getStrippedFilePath($file)
	{
		return $this->stripPathRegex ? preg_replace($this->stripPathRegex, '', $file) : $file;
	}

	/**
	 * Set your app's semantic version, eg "1.2.3".
	 *
	 * @param string|null $appVersion the app's version
	 *
	 * @return $this
	 */
	public function setAppVersion($appVersion)
	{
		$this->appData['version'] = $appVersion;

		return $this;
	}

	/**
	 * Set your release stage, eg "production" or "development".
	 *
	 * @param string|null $releaseStage the app's current release stage
	 *
	 * @return $this
	 */
	public function setReleaseStage($releaseStage)
	{
		$this->appData['releaseStage'] = $releaseStage;

		return $this;
	}

	/**
	 * Set the type of application executing the code.
	 *
	 * This is usually used to represent if you are running plain PHP code
	 * "php", via a framework, eg "laravel", or executing through delayed
	 * worker code, eg "resque".
	 *
	 * @param string|null $type the current type
	 *
	 * @return $this
	 */
	public function setAppType($type)
	{
		$this->appData['type'] = $type;

		return $this;
	}

	/**
	 * Set the fallback application type.
	 *
	 * This is should be used only by libraries to set an fallback app type.
	 *
	 * @param string|null $type the fallback type
	 *
	 * @return $this
	 */
	public function setFallbackType($type)
	{
		$this->fallbackType = $type;

		return $this;
	}

	/**
	 * Get the application data.
	 *
	 * @return array
	 */
	public function getAppData()
	{
		return array_merge(array_filter(['type' => $this->fallbackType, 'releaseStage' => 'production']),
			array_filter($this->appData));
	}

	/**
	 * Set UkieReport's error reporting level.
	 *
	 * If this is not set, we'll use your current PHP error_reporting value
	 * from your ini file or error_reporting(...) calls.
	 *
	 * @param int|null $errorReportingLevel the error reporting level integer
	 *
	 * @return $this
	 */
	public function setErrorReportingLevel($errorReportingLevel)
	{
		$this->errorReportingLevel = $errorReportingLevel;

		return $this;
	}

	/**
	 * Should we ignore the given error code?
	 *
	 * @param int $code the error code
	 *
	 * @return bool
	 */
	public function shouldIgnoreErrorCode($code)
	{
		if (isset($this->errorReportingLevel)) {
			return !($this->errorReportingLevel & $code);
		}

		return !(error_reporting() & $code);
	}
}
