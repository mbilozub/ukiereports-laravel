<?php

return [
	'enable' => env('UKIEREPORTS_ENABLE', false),
	'api_url' => env('UKIEREPORTS_URL'),
	'api_secret' => env('UKIEREPORTS_TOKEN')
];