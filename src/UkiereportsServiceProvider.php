<?php

namespace Mbilozub\UkieReports\Laravel;

use Illuminate\Support\ServiceProvider;

class UkiereportsServiceProvider extends ServiceProvider
{
	protected $defer = false;

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->publishes([
			__DIR__ . '/config/config.php' => config_path('ukiereports.php'),
		]);
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton('ukiereports', function ($app) {
			return new Report();
		});
		$this->mergeConfigFrom(
			__DIR__ . '/config/config.php', 'ukiereports'
		);
	}
}